role :ssh, File.read('hosts').split

namespace :vim do
  task :make_dirs do
    on roles(:all), in: :parallel do |host|
      execute "mkdir -p ~/.vim/autoload ~/.vim/plugged"
    end
  end
  task :install_plug => :make_dirs do
    on roles(:all), in: :parallel do |host|
      execute "curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
      https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
    end
  end
  task :run_pluginstall => :install_plug do
    on roles(:all), in: :parallel do |host|
      execute "vim +PlugInstall +qall"
    end
  end
  task :copy_vimrc do
    on roles(:all), in: :parallel do |host|
      upload! File.join(ENV['HOME'], '.vimrc'), ".vimrc"
    end
  end
  task :setup => [:copy_vimrc, :run_pluginstall] do
  end
end
