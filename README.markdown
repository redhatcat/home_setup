Tyler's Home Prep
=================

This is my little utility for setting up my home on other boxes via
capistrano. At the moment, it is concerned with vim.

What it does on each of your **hosts**:

* Installs [vim-plug](https://github.com/junegunn/vim-plug)
* Runs PlugInstall to install your defined vim plugins
* Uploads your local ~/.vimrc

Setup
-----

    git clone redhatcat git@bitbucket.org:redhatcat/home_setup.git
    cd home_setup
    bundle install
    cat "mybox.com anotherbox.org yetanother.net" > hosts

Your **hosts** file specifies all the boxes you would like to setup on,
separated by any kind of whitespace.


Running
-------

    cap vim:setup
